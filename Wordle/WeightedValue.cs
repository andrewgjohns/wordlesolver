﻿namespace Wordle
{
    public class WeightedValue
    {
        public WeightedValue(string word, int value)
        {
            Word = word;
            Value = value;
        }
        public string Word { get;  }
        public int Value { get;  }
    }
}