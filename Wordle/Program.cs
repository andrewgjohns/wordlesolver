﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Wordle
{
    class Program
    {
        private static int wordSize = 5;
        private static int wordsPerLine = 10;
        // https://www-cs-faculty.stanford.edu/~knuth/sgb.html
        private static string wordFile = @"resources/sgb-words.txt";
        static void Main(string[] args)
        {
            Console.WriteLine("Starting ....");
            var words = GetWords();
            Console.WriteLine($"Got words: {words.Count}");

            Start(words);
        }
        
        private static List<string> GetWords()
        {
            var words = new List<string>();
            foreach (var line in File.ReadLines(wordFile))
            {
                var word = line.Trim().ToLower();
                words.Add(word);
            }
            return words;
        }

        private static void Start(IReadOnlyCollection<string> words)
        {
            while (true)
            {
                Console.Write("Contains: (lowercase char(s) no spaces. Ex: abc): ");
                var contains = Console.ReadLine() ?? "";

                Console.Write("Excludes: (lowercase char(s) no spaces. Ex: abc): ");
                var exclude = Console.ReadLine() ?? "";
                var excludePattern = $"[{exclude.ToLower()}]";

                Console.Write("Pattern: (Example word is agree, ar are known. Ex: a.r..): ");
                var pattern = (Console.ReadLine() ?? "").ToLower();

                var antiPatterns = new List<string>();
                while (true)
                {
                    Console.Write("AntiPattern: (Example you know the second letter is not w. Ex: .w...): ");
                    var antiPattern = Console.ReadLine() ?? "";
                    if (string.IsNullOrEmpty(antiPattern.ToLower()))
                    {
                        break;
                    }
                    antiPatterns.Add(antiPattern);
                }

                var remainingWords = words.Where(w =>
                {
                    if (!string.IsNullOrEmpty(contains) && contains.Any(c => !w.Contains(c)))
                    {
                        return false;
                    }
                    
                    if (!string.IsNullOrEmpty(exclude) && Regex.IsMatch(w, excludePattern))
                    {
                        return false;
                    }
                    
                    if (antiPatterns.Any(anti => Regex.IsMatch(w, anti)))
                    {
                        return false;
                    }

                    return string.IsNullOrEmpty(pattern) || Regex.IsMatch(w, pattern);
                }).ToList();

                var weightedWords = Weight(remainingWords).OrderByDescending(w => w.Value).ToList();

                Console.WriteLine();
                Console.WriteLine($"Suggestions: ({remainingWords.Count})");
                var line = new string('-', wordsPerLine*5+4*8);
                Console.WriteLine(line);
                var counter = 0;
                foreach (var word in weightedWords)
                {
                    // if (counter == wordsPerLine)
                    // {
                    //     Console.WriteLine();
                    //     counter = 0;
                    // }
                    // Console.Write($"{word}\t");
                    // counter++;
                    
                    Console.WriteLine($"{word.Word} - {word.Value}");
                }
                Console.WriteLine();
                Console.WriteLine(line);
                Console.Write("Try again [Yy/Nn]:");
                var next = Console.ReadLine() ?? "";

                if (next.ToLower() != "y")
                {
                    Console.WriteLine("Done.");
                    break;
                }
            }
            
            Console.WriteLine();
        }

        private static List<WeightedValue> Weight(IReadOnlyCollection<string> words)
        {
            var values = new List<WeightedValue>();
            var wordCount = words.Count - 1;

            if (wordCount == 0)
            {
                values.Add( new WeightedValue(words.First(), 100));
                return values;
            }

            foreach (var word in words)
            {
                var key = word;
                var baseValue = (key.Distinct().Count() - wordSize);
                var wordValue = 0;
                foreach (var otherWord in words)
                {
                    if (key == otherWord)
                    {
                        continue;
                    }

                    wordValue += key
                        .Distinct()
                        .Count(letter => otherWord.Contains(letter));
                }

                var value = (wordValue / wordCount) + baseValue;
                values.Add(new WeightedValue(key, value));
            }

            return values;
        }
    }
}